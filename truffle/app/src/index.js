import Web3 from "web3";
import mmtArtifact from "../../build/contracts/MMTtoken.json";
import languageValuesinTruffle from "../../build/contracts/language.json";

var dataTimer = null;
let currentAddress;
var dividendValue = 0;
var tokenBalance = 0;
let actual_JSON;
var ethPrice = 0;
var ethPriceTimer = null;
let ethTaiwanPrice = 0;
let ethTaiwanPriceTimer = null;
var currency = (typeof default_currency === 'undefined') ? 'USD' : default_currency;
var buyPrice = 0;
var globalBuyPrice = 0;
let contract;
var sellPrice = 0;
var infoTimer = null;
var contractAddress;



function convertWeiToEth(e) {
  return e / 1e18
}


function convertEthToWei(e) {
  return 1e18 * e
}




function getCookie(name) {
  var dc = document.cookie;
  var prefix = name + "=";
  var begin = dc.indexOf("; " + prefix);

  if (begin == -1) {
    begin = dc.indexOf(prefix);
    if (begin != 0) return null;
  } else {
    begin += 2;
    var end = document.cookie.indexOf(";", begin);
    if (end == -1) {
      end = dc.length;
    }
  }

  return decodeURI(dc.substring(begin + prefix.length, end));
}



//appends an "active" class to .popup and .popup-content when the "Open" button is clicked
// $(".open").on("click", function(){
//   $(".popup-overlay, .popup-content").addClass("active");
// });

//removes the "active" class to .popup and .popup-content when the "Close" button is clicked 
$(".close, .popup-overlay").on("click", function () {
  $(".popup-overlay, .popup-content").removeClass("active");
});



function openEtherscan() {
  window.open("https://etherscan.io/address/" + contractAddress, "_blank");
}

var lang = localStorage.getItem("language");
console.log(lang);
let langValues;

$(function() {

    function loadJSON(callback) {   
      console.log(languageValuesinTruffle);

            var xobj = new XMLHttpRequest();
                xobj.overrideMimeType("application/json");
            xobj.open('GET', 'language.json', true); 
            xobj.onreadystatechange = function () {
                if (xobj.readyState == 4 && xobj.status == "200") {
                    callback(JSON.parse(xobj.responseText));
                }
            };
            callback(languageValuesinTruffle);  
         }

     loadJSON(function(response) {
        langValues = response;
       });
 
})

const App = {
  web3: null,
  account: null,
  meta: null,

  start: async function() {
    const { web3 } = this;

    try {
      // get contract instance
      const networkId = await web3.eth.net.getId();
      console.log(networkId);
      const deployedNetwork = mmtArtifact.networks[networkId];
      console.log(deployedNetwork);
      contractAddress = deployedNetwork.address;
      this.meta = new web3.eth.Contract(
        mmtArtifact.abi,
        deployedNetwork.address,
      );

      // get accounts
      const accounts = await web3.eth.getAccounts();
      console.log(accounts);
      this.account = accounts[0];
      console.log(this.account);

      this.updateEthPrice();
      this.refreshTable();
    } catch (error) {
      console.error("Could not connect to contract or chain.");
    }
  },

  updateEthPrice: async function () {

    const result = await $.getJSON('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=ethereum');
    var eth = result[0];
    ethPrice = parseFloat(eth['current_price']);

    const resultOfTainwanCurrency = await $.getJSON('https://api.coingecko.com/api/v3/coins/markets?vs_currency=twd&ids=ethereum');
    var taiwanCurrencyResult = resultOfTainwanCurrency[0];
    ethTaiwanPrice = parseFloat(taiwanCurrencyResult['current_price']);

    ethPriceTimer = setTimeout(this.updateEthPrice, 1000)

    $("#present-eth-nt-exchangeRate").text(ethTaiwanPrice);
  },


  refreshTable: async function () {


    web3.eth.getBalance(contractAddress, function (e, r) {
      $('.contract-balance').text(convertWeiToEth(r).toFixed(4) + " ETH")
      $('.contract-balance-usd').text('(' + Number((convertWeiToEth(r) * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')');
      $('.contract-balance-twd').text('(' + Number((convertWeiToEth(r) * ethTaiwanPrice).toFixed(2)).toLocaleString() + ' ' + 'NT$' + ')');

    })

    const {
      totalSupply,
      tokensForOneEther,
      calculateTokensReceived,
      seeMyDividend,
      balanceOf,
      payoutsTo_,
      minimumPurchaseToken,
      OwnersAndFriendsSeeDividends,
      OwnerAndFriendsSeeTheirTokensInPool,
      adminsAlreadyConvertedTokensToEth,
    } = this.meta.methods;
 
    // total supply
    const totalCoinsInSupply = await totalSupply().call();
    $('.contract-tokens').text(totalCoinsInSupply / 100); //two decimal Token

    //buy price
    const tokensFor1Eth = await tokensForOneEther().call();
    console.log("see", tokensFor1Eth);
    let buyPrice = 1 / tokensFor1Eth;
    globalBuyPrice = 1 / tokensFor1Eth;
    $('.buy').text(buyPrice.toFixed(6) + ' ETH')
    $('.buy-usd').text('(' + Number((buyPrice * ethPrice).toFixed(2)).toLocaleString() + ' ' + currency + ')')
    $('.buy-twd').text('(' + Number((buyPrice * ethTaiwanPrice).toFixed(2)).toLocaleString() + ' ' + 'NT$' + ')')
    //estimate tokens for input ether
    $('#purchase-amount').bind("keypress keyup click", async function (e) {
      var number = $('#purchase-amount').val();
      var value = parseFloat(number);
      if (value === 0 || Number.isNaN(value)) {
        $('#deposit-hint').text("");
        return;
      }
      if (value > 0) {
        //this is an udayip done to overcome an issue with web3, we amplify it inot large number, get large number result and divide it back to make it normal
        var valueToEstimate = value * 1e18;
        let tokens = await calculateTokensReceived(web3.toWei(valueToEstimate, 'ether')).call();
        var tokensBackToRealNumber = convertWeiToEth(tokens);
             
        var key = 'approximateTokensForInputEth';
        var message = langValues[lang][key];
         
        $('#deposit-hint').html(message + ' '+ tokensBackToRealNumber / 100 );
         //Two decimal Token, Hence / 100
      }
    })

    //see profit or dividend of user
    const userProfit = await seeMyDividend(this.account).call();
    $('.profit-value').text(convertWeiToEth(userProfit));
    $('.profit-value-usd').text(Number((convertWeiToEth(userProfit) * ethPrice).toFixed(2)).toLocaleString())
    $('.profit-value-twd').text(Number((convertWeiToEth(userProfit) * ethTaiwanPrice).toFixed(2)).toLocaleString())
    //user tokens in hand (balance)
    const userTokenBalance = await balanceOf(this.account).call();
    var userTokenBalanceWithDecimal = Number(userTokenBalance / 100);
    $('.balance').text(userTokenBalanceWithDecimal);
    let tokenOwnedValueInEth = globalBuyPrice * userTokenBalanceWithDecimal;
    $('.value').text(tokenOwnedValueInEth);
    $('.value-usd').text(Number((tokenOwnedValueInEth * ethPrice).toFixed(2)).toLocaleString())
    $('.value-twd').text(Number((tokenOwnedValueInEth * ethTaiwanPrice).toFixed(2)).toLocaleString())

    //user already earned profits
    const payOutsAlreadyReceived = await payoutsTo_(this.account).call();
    console.log("payouts", payOutsAlreadyReceived);
    let convertedPayOutToEthFromWei = convertWeiToEth(payOutsAlreadyReceived);
    $('.div').text(convertedPayOutToEthFromWei)
    $('.div-usd').text(Number((convertedPayOutToEthFromWei * ethPrice).toFixed(2)).toLocaleString())
    $('.div-twd').text(Number((convertedPayOutToEthFromWei * ethTaiwanPrice).toFixed(2)).toLocaleString())

    //upating same for admin
    $('.div-admin-consumed').text(convertedPayOutToEthFromWei);
    $('.div-admin-usd-consumed').text(Number((convertedPayOutToEthFromWei * ethPrice).toFixed(2)).toLocaleString())
    $('.div-admin-twd-consumed').text(Number((convertedPayOutToEthFromWei * ethTaiwanPrice).toFixed(2)).toLocaleString())



    //admin level function

    //setting minimum coin requirement

    const minimumCoinPurchase = await minimumPurchaseToken().call();

    $('#staking-amount').on('input change', function () {
      var value = parseFloat($(this).val());

      if (value === 0 || Number.isNaN(value)) {
        $('#deposit-hint').text("");
        return;
      }

      if (value > 0) {
        let tokens = convertWeiToEth(minimumCoinPurchase / 100);
        $('#staking-hint').text("The Present Minimum Amount of Tokens is " + tokens.toFixed(0) + " tokens!");

      }

    });

    $('#token-amount-one-twd').on('input change', function () {
      var value = parseFloat($(this).val());

      if (value === 0 || Number.isNaN(value)) {
        $('#deposit-hint').text("");
        return;
      }

      if (value > 0) {
        $('#present-coin-for-1-twd-hint').text("The Present Amount of Tokens is " + tokensFor1Eth + " tokens for 1 Ether!");
      }

    });


    //see admin profit / dividend
    const adminProfit = await OwnersAndFriendsSeeDividends(this.account).call();
    console.log("this is adminProfit", adminProfit);
    $('.profit-value-admin').text(convertWeiToEth(adminProfit));
    $('.profit-value-admin-usd').text(Number((convertWeiToEth(adminProfit) * ethPrice).toFixed(2)).toLocaleString())
    $('.profit-value-admin-twd').text(Number((convertWeiToEth(adminProfit) * ethTaiwanPrice).toFixed(2)).toLocaleString())

    const totalTokensOfAdmin = await OwnerAndFriendsSeeTheirTokensInPool(this.account).call();
    $('.token-balance-admin').text(totalTokensOfAdmin / 100);

    const adminsBurnedTokensTokens = await adminsAlreadyConvertedTokensToEth(this.account).call();
    $('.token-burned-admin').text(adminsBurnedTokensTokens / 100);

  },

  buyTokens: async function () {

    const {
      buy,
      hasUserPurchasedBefore,
      minimumPurchaseToken,
      calculateTokensReceived
    } = this.meta.methods;

    let amount = $('#purchase-amount').val().trim()

    if (amount <= 0 || !isFinite(amount) || amount === '') {

      let key = 'validAmount';
      let message = langValues[lang][key];

      $("#errorMsg").html(message);
      $(".popup-overlay, .popup-content").addClass("active");

    } else {

      let userAlreadyPurchased = await hasUserPurchasedBefore(this.account).call();

      if (userAlreadyPurchased) {

        let key = 'alreadyInvested';
        let message = langValues[lang][key];

        $("#errorMsg").html(message);
        $(".popup-overlay, .popup-content").addClass("active");

      } else {

        var valueToEstimate = amount * 1e18;
        let tokensForInputEth = await calculateTokensReceived(web3.toWei(valueToEstimate, 'ether')).call();
        console.log("tokens for input eth 1", tokensForInputEth);
        var tokensBackToRealNumberForInputEth = convertWeiToEth(tokensForInputEth) / 100;

        const minimumCoinPurchase = await minimumPurchaseToken().call();
        let tokensThatHasToBePurchasedMinumum = convertWeiToEth(minimumCoinPurchase / 100);

        console.log("tokens for input eth", tokensBackToRealNumberForInputEth);
        console.log("minimum tokens", tokensThatHasToBePurchasedMinumum);

        if (tokensBackToRealNumberForInputEth >= tokensThatHasToBePurchasedMinumum) {

          await buy().send({
            from: this.account,
            value: convertEthToWei(amount)
          });

          location.reload();

        } else {

          $("#errorMsg").html("You Have To Purchase a minimum of " + tokensThatHasToBePurchasedMinumum);
          $(".popup-overlay, .popup-content").addClass("active");

        }



      }

    }

  },

  withdraw: async function () {

    const {
      seeMyDividend,
      withdraw
    } = this.meta.methods;

    const userProfit = await seeMyDividend(this.account).call();

    if (userProfit > 0) {
      await withdraw().send({
        from: this.account
      });
    } else {
      $("#errorMsg").html("You Donot Have Any Profit To Withdraw!");
      $(".popup-overlay, .popup-content").addClass("active");

    }

    location.reload();

  },

  transferCoins: async function () {

    const {
      transfer
    } = this.meta.methods;

    let address = $('#transfer-address').val();
    let amount = $('#transfer-tokens').val();

    if (web3.isAddress(address) && parseFloat(amount)) {
      // var amountConvertedToWei = convertEthToWei(amount);
      var amountConvertedToCoverDecimalPlaces = amount * 100;
      await transfer(address, amountConvertedToCoverDecimalPlaces).send({
        from: this.account
      });

    } else {
      console.log("Invalid Address / Amount");
      $("#errorMsg").html("Invalid Address / Amount");
      $(".popup-overlay, .popup-content").addClass("active");
    }

    location.reload();

  },

  //admin functions

  updateMinimumToken: async function () {


    const {
      setStakingRequirement
    } = this.meta.methods;

    let amount = $('#staking-amount').val().trim();
    if (amount <= 0 || !isFinite(amount) || amount === '') {
      $("#errorMsg").html("Plese enter valid number of Tokens");
      $(".popup-overlay, .popup-content").addClass("active");
    } else {
      await setStakingRequirement(web3.toWei(amount * 100)).send({
        from: this.account
      });
    }

    location.reload();
  },

  updateCoinFor1Eth: async function () {

    const {
      changeTokensForOneEther
    } = this.meta.methods;

    let amount = $('#token-amount-one-twd').val().trim();
    if (amount <= 0 || !isFinite(amount) || amount === '') {
      $("#errorMsg").html("Plese enter valid number of Tokens");
      $(".popup-overlay, .popup-content").addClass("active");
    } else {
      console.log(amount);
      await changeTokensForOneEther(Math.round(amount)).send({
        from: this.account
      });
    }

    location.reload();

  },

  withdrawAdmin: async function () {

    const {
      OwnersAndFriendsSeeDividends,
      ownerAndFriendsWithdrawDividends
    } = this.meta.methods;

    const adminProfit = await OwnersAndFriendsSeeDividends(this.account).call();

    if (adminProfit > 0) {
      await ownerAndFriendsWithdrawDividends().send({
        from: this.account
      });
    } else {
      $("#errorMsg").html("You Donot Have Any Profit To Withdraw!");
      $(".popup-overlay, .popup-content").addClass("active");

    }

    location.reload();


  },

   setAdminPercentage: async function () {

    const {
      updatePercentageOfAdministrator
    } = this.meta.methods;

       let adminAddress = $('#addressOfAdmintoSetPercentage').val(); 
       let percentage = $('#inputPeercentageToSet').val();

        if (web3.isAddress(adminAddress)) {

          await updatePercentageOfAdministrator(adminAddress, percentage).send({
            from: this.account
          });

    } else {
      console.log("Invalid Address / Percentage");
      $("#errorMsg").html("Invalid Address / Amount");
      $(".popup-overlay, .popup-content").addClass("active");
    }

    location.reload();
  }
};

window.App = App;

window.addEventListener("load", function() {
  if (window.ethereum) {
    // use MetaMask's provider
    App.web3 = new Web3(window.ethereum);
    window.ethereum.enable(); // get permission to access accounts

    // window.ethereum.on('accountsChanged', function (accounts) {
    //   location.reload();
    // })  


  } else {
    console.warn(
      "No web3 detected. Falling back to http://127.0.0.1:8545. You should remove this fallback when you deploy live",
    );
    // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
    App.web3 = new Web3(
      new Web3.providers.HttpProvider("http://127.0.0.1:8545"),
    );
  }

   

  App.start();
});

