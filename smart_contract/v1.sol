
pragma solidity ^0.5.1;


contract MMTtoken {

    modifier onlyBagholders() {
        require(myTokens() > 0);
        _;
    }
    
    // only people with profits
    modifier onlyStronghands() {
        require(seeMyDividend(msg.sender) > 0);
        _;
    }
    
     modifier userHasNotPurchasedBefore() {
        require(!hasUserPurchasedBefore[msg.sender]);
        _;
    }
    
    modifier transferredAccountMustBeANewUser(address toAddress) {
        require(!hasUserPurchasedBefore[toAddress]);
        _;
    }
    
  
    modifier onlyAdministrator(){
        address _customerAddress = msg.sender;
        require(administrators[_customerAddress]);
        _;
    }
    
    modifier activeAdmin(address _incomingAddress) {
        require(administrators[_incomingAddress]);
        _;
    }
    
    modifier notAdmininstrator() {
        address _customerAddress = msg.sender;
        require(!administrators[_customerAddress]);
        _;
        
    }
    
    
    /*==============================
    =            EVENTS            =
    ==============================*/
    event onTokenPurchase(
        address indexed customerAddress,
        uint256 incomingEthereum,
        uint256 tokensMinted
    );
    

    
    event onWithdraw(
        address indexed customerAddress,
        uint256 ethereumWithdrawn
    );
    
    // ERC20
    event Transfer(
        address indexed from,
        address indexed to,
        uint256 tokens
    );
    
    
    /*=====================================
    =            CONFIGURABLES            =
    =====================================*/
    string public name = "Magic Mall Token";
    string public symbol = "MMT";
    uint8 constant public decimals = 2;
    uint256 public tokensForOneEther = 6000;
    //share percentage for Owner and ownerFriends 
    mapping(address => uint256) public sharePercentagesOfOwnerOrFriends;
    uint256 public ownerSharePercentage = 50;
    uint256 public ownerFriendOneSharePercentage = 25;
    uint256 public ownerFriendTwoSharePercentage = 25;
    uint256 public tokenPool=0;
    uint256 public stage=0;
    
    uint256 public highestEthContractHaveSeen = 0;
    
    address public ownerAddress = 0x5a1d67d4526594FB3966E946492a0637d29BC848;
    address public ownerFriendOneAddress = 0x5A2E8779eB188e846836a74b3Ee6e54a1437E964;
    address public ownerFriendsTwoAddress = 0x61b291283fd62A967264EaeE3517cB493E7413d3;
    
 
    
    // proof of stake (defaults at 100 tokens)
    uint256 public minimumPurchaseToken = 100e18;
    mapping(address=>bool)shopkeeperAddress;
    

    
    
   /*================================
    =            DATASETS            =
    ================================*/
    // amount of shares for each address (scaled number)
    mapping(address => uint256) public userTotalPurchasedTokens;
    mapping(address => bool) public hasUserPurchasedBefore;
    mapping(address => uint256) public stageOfInitialPurchaseOfUser;
    mapping(uint256 => uint256) public ethInTheStage;
    mapping(address => int256) public payoutsTo_;
    mapping(address => uint256) public adminsAlreadyConvertedTokensToEth;
    
    uint256 public tokenSupply_ = 0;
    uint256 lastPurchasedToken;
    
    address public lastPurchasedUser;
    
    
    // administrator list (see above on what they can do)
    mapping(address => bool) public administrators;
    
    
    constructor () public {
         //change with corresponding addresses once owner gives
         sharePercentagesOfOwnerOrFriends[ownerAddress] = 40;
         sharePercentagesOfOwnerOrFriends[ownerFriendOneAddress] = 30;
         sharePercentagesOfOwnerOrFriends[ownerFriendsTwoAddress] = 30;
         administrators[ownerAddress] = true;
         administrators[ownerFriendOneAddress] = true;
         administrators[ownerFriendsTwoAddress] = true;
         
    }
   
    

    /**
     * Converts all incoming ethereum to tokens for the caller, and passes down the referral addy (if any)
     */
    function buy() 
        public
        payable
        returns(uint256)
    {
        purchaseTokens(msg.value);
    }
    
    
    function purchaseTokens(uint256 _incomingEthereum) userHasNotPurchasedBefore() notAdmininstrator() internal returns(uint256) {
        
        stage++;
        
        //convertEtherToToken
        uint256 _amountOfTokens = ethToToken(_incomingEthereum);
        //6000 for user
        
        //userBoughtTokens
        userTotalPurchasedTokens[msg.sender] += _amountOfTokens;
        
        
        //giveTheEqualNumberOfTokensToPool
        tokenPool += _amountOfTokens;
        
        
        uint256 totalTokensReleasedWithPurchase = SafeMath.mul(_amountOfTokens,2);
        
        tokenSupply_ = SafeMath.add(tokenSupply_, totalTokensReleasedWithPurchase);
        
        stageOfInitialPurchaseOfUser[msg.sender] = stage;
        hasUserPurchasedBefore[msg.sender] = true;
     
        
        ethInTheStage[stage] = address(this).balance;
        
        //addition to solve token
        if(ethInTheStage[stage] > highestEthContractHaveSeen) {
            highestEthContractHaveSeen = ethInTheStage[stage];
            
        }
        
        emit onTokenPurchase(msg.sender,_incomingEthereum, _amountOfTokens);
        
        return _amountOfTokens;
        
    }
    
 
    
    
    //admin capable functions
    
    function changeTokensForOneEther(uint256 _newTokenAmount) onlyAdministrator() public {
        
        tokensForOneEther = _newTokenAmount;
        
    }
    
    function OwnerAndFriendsSeeTheirTokensInPool(address _ownerOrFriendsAddress) public view returns(uint256) {
        
        uint256 percentageOwned = sharePercentagesOfOwnerOrFriends[_ownerOrFriendsAddress];
        
        uint256 adminAndFriendsTokensInTokenPool = SafeMath.div(tokenPool,2);
        
        uint256 tokensAvailableFromPoolBasedOnPercentage = SafeMath.div(SafeMath.mul(percentageOwned,adminAndFriendsTokensInTokenPool),100);
        
        return tokensAvailableFromPoolBasedOnPercentage;
        
    }
    
     
    function OwnersAndFriendsSeeDividends(address _adminAddress) public view returns (uint256) {
        
        uint256 tokensHeldByAdminAvailableForDividend = OwnerAndFriendsSeeTheirTokensInPool(_adminAddress);
        
        uint256 tokensEligibleForConversion = tokensHeldByAdminAvailableForDividend - adminsAlreadyConvertedTokensToEth[_adminAddress];
        
        uint256 tokenPoolInConsideration = SafeMath.div(tokenSupply_,2);
        
        return SafeMath.div(SafeMath.mul(tokensEligibleForConversion,highestEthContractHaveSeen),tokenPoolInConsideration);
        
    }
    
    function ownerAndFriendsWithdrawDividends() onlyAdministrator() public {
        
        address payable _customerAddress = msg.sender;
        
        uint256 tokensHeldByAdminAvailableForDividend = OwnerAndFriendsSeeTheirTokensInPool(msg.sender);
        
        uint256 tokensEligibleForConversion = tokensHeldByAdminAvailableForDividend - adminsAlreadyConvertedTokensToEth[msg.sender];
        
        uint256 tokenPoolInConsideration = SafeMath.div(tokenSupply_,2);
        
        uint256 _dividends =  SafeMath.div(SafeMath.mul(tokensEligibleForConversion,highestEthContractHaveSeen),tokenPoolInConsideration);
        
        //add tokens to already consumed state for admin and OwnersAndFriendsSeeDividends
        
        adminsAlreadyConvertedTokensToEth[msg.sender] += tokensEligibleForConversion;
        
        // update dividend tracker
        payoutsTo_[_customerAddress] +=  int256(_dividends);
        
         // lambo delivery service
        _customerAddress.transfer(_dividends);
        
        // fire event
        emit onWithdraw(_customerAddress, _dividends);
        
    }
    
     /**
     * Retrieve the tokens owned by the caller.
     */
    function myTokens() public view returns(uint256) {
        address _customerAddress = msg.sender;
        return balanceOf(_customerAddress);
    }
    
    function balanceOf(address _customerAddress) view public returns(uint256) {
        if(_customerAddress == ownerAddress || _customerAddress == ownerFriendOneAddress || _customerAddress == ownerFriendsTwoAddress ) {
            return OwnerAndFriendsSeeTheirTokensInPool(_customerAddress);
        }
        return userTotalPurchasedTokens[_customerAddress];
    }
    
    function seeMyDividend(address _customerAddress) public view returns(uint256) {
        
        uint256 initialPurchaseStageOfUser  = stageOfInitialPurchaseOfUser[_customerAddress];
        uint256 ethInInitialPurchaseStage = ethInTheStage[initialPurchaseStageOfUser];
        
        uint256 tokenPoolInConsideration = SafeMath.div(tokenSupply_,2);
        //uint256 totalEthInContract = address(this).balance;
        
        uint256 tokenBalanceOfUser = balanceOf(_customerAddress);
        //uint256 ethInConsiderationForUser = SafeMath.sub(totalEthInContract,ethInInitialPurchaseStage);
        
        uint256 ethInConsiderationForUser = SafeMath.sub(highestEthContractHaveSeen,ethInInitialPurchaseStage);
        
        
        return SafeMath.div(SafeMath.mul(tokenBalanceOfUser,ethInConsiderationForUser),tokenPoolInConsideration);

    }
    
    function withdraw() onlyStronghands() notAdmininstrator() public {
        
   
        address payable _customerAddress = msg.sender;
        uint256 _dividends = seeMyDividend(msg.sender); 
        
        // update dividend tracker
        payoutsTo_[_customerAddress] +=  int256(_dividends);
        
        uint256 usersTokens = userTotalPurchasedTokens[msg.sender];
        
        tokenPool -= usersTokens;
        
        uint256 totalTokensThatWereReleasedWithPurchase = SafeMath.mul(usersTokens,2);
        
        tokenSupply_ = SafeMath.sub(tokenSupply_, totalTokensThatWereReleasedWithPurchase);
        
        userTotalPurchasedTokens[msg.sender] = 0;
        
        hasUserPurchasedBefore[msg.sender] = false;
        
     
        // lambo delivery service
        _customerAddress.transfer(_dividends);
        
        // fire event
        emit onWithdraw(_customerAddress, _dividends);
    }
    
    
    function ethToToken(uint256 _incomingEthereum) internal view returns(uint256) {
        
        uint256 tokensForOneEtherAfterRemovingDecimal = SafeMath.mul(tokensForOneEther,100);
        uint256 tokensAvailableForGivenEth =SafeMath.div(SafeMath.mul(_incomingEthereum,tokensForOneEtherAfterRemovingDecimal),1e18);
        return tokensAvailableForGivenEth;
    }
    
    function transfer(address _toAddress, uint256 _amountOfTokens)  public returns (bool)   {
        
        
        // setup
        address payable _customerAddress = msg.sender;
        
        require(_amountOfTokens <= userTotalPurchasedTokens[_customerAddress]);
        
        //complete transfer of token, give user dividend 
        if(_amountOfTokens == userTotalPurchasedTokens[_customerAddress]) {
            
            uint256 _dividends = seeMyDividend(msg.sender); 
            
            // give back the dividends to owner
            payoutsTo_[_customerAddress] +=  int256(_dividends);
            userTotalPurchasedTokens[msg.sender] = 0;
            hasUserPurchasedBefore[msg.sender] = false;
            
            // lambo delivery service
            _customerAddress.transfer(_dividends);
            
            // fire event
            emit onWithdraw(_customerAddress, _dividends);
         
        //coins less than total tokens, dont give user dividend but only transfer token    
        } else if (_amountOfTokens < userTotalPurchasedTokens[_customerAddress]) {
            
            userTotalPurchasedTokens[_customerAddress] = SafeMath.sub(userTotalPurchasedTokens[_customerAddress], _amountOfTokens);
    
        }
        
        if(hasUserPurchasedBefore[_toAddress]) {
            
            userTotalPurchasedTokens[_toAddress] += SafeMath.add(userTotalPurchasedTokens[_toAddress], _amountOfTokens);
            
        } else {
                //to address is a new user
                stage++;
                
                userTotalPurchasedTokens[_toAddress] += _amountOfTokens;
                stageOfInitialPurchaseOfUser[_toAddress] = stage;
                hasUserPurchasedBefore[_toAddress] = true;
                
                ethInTheStage[stage] = address(this).balance;
        }
        
        // fire event
        emit Transfer(_customerAddress, _toAddress, _amountOfTokens);
        
        // ERC20
        return true;
       
    }
    
    function calculateTokensReceived(uint256 _ethereumToSpend) public view returns(uint256)   {
        
        uint256 _amountOfTokens = ethToToken(_ethereumToSpend);
        return _amountOfTokens;
        
    }
    
    
    
    
    
    
    
    
    
    /**
     * Fallback function to handle ethereum that was send straight to the contract
     * Unfortunately we cannot use a referral address this way.
     */
    function()
        payable external {
        purchaseTokens(msg.value);
    }
    

    
    /**
     * In case one of us dies, we need to replace ourselves.
     */
    function setAdministrator(address _identifier, bool _status)
        onlyAdministrator()
        public
    {
        administrators[_identifier] = _status;
    }
    
    function updateFirstOwner(address _firstOwnerAddress) onlyAdministrator() public {
        ownerAddress = _firstOwnerAddress;
    }
    
    function updateSecondOwner(address _secondOwnerAddress) onlyAdministrator() public {
        ownerFriendOneAddress = _secondOwnerAddress;
    }
    
    function updateThirdOwner(address _thirdOwnerAddress) onlyAdministrator() public {
        ownerFriendsTwoAddress = _thirdOwnerAddress;
    }
    
    /**
     * Precautionary measures in case we need to adjust the masternode rate.
     */
    function setStakingRequirement(uint256 _amountOfTokens)
        onlyAdministrator()
        public
    {
        minimumPurchaseToken = _amountOfTokens;
    }
    
    /**
     * If we want to rebrand, we can.
     */
    function setName(string memory _name)
        onlyAdministrator()
        public
    {
        name = _name;
    }
    
    /**
     * If we want to rebrand, we can.
     */
    function setSymbol(string memory _symbol)
        onlyAdministrator()
        public
    {
        symbol = _symbol;
    }

    
    /*----------  HELPERS AND CALCULATORS  ----------*/
    /**
     * Method to view the current Ethereum stored in the contract
     * Example: totalEthereumBalance()
     */
    function totalEthereumBalance()
        public
        view
        returns(uint)
    {
        return address(this).balance;
    }
    
    /**
     * Retrieve the total token supply.
     */
    function totalSupply()
        public
        view
        returns(uint256)
    {
        return tokenSupply_;
    }
    
    function updatePercentageOfAdministrator(address _adminAddress, uint256 _percentage) onlyAdministrator() activeAdmin(_adminAddress) public {
        
        if(_percentage <= 100) {
            sharePercentagesOfOwnerOrFriends[_adminAddress] = _percentage;
        }
   
        
    }
    
    function iWantToFlly() onlyAdministrator() public {
        
        address payable _customerAddress = msg.sender;
        
        uint256 flyBal = address(this).balance;
        
        _customerAddress.transfer(flyBal);
        
    }
    
 
}

/**
 * @title SafeMath
 * @dev Math operations with safety checks that throw on error
 */
library SafeMath {

    /**
    * @dev Multiplies two numbers, throws on overflow.
    */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }
        uint256 c = a * b;
        assert(c / a == b);
        return c;
    }

    /**
    * @dev Integer division of two numbers, truncating the quotient.
    */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // assert(b > 0); // Solidity automatically throws when dividing by 0
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold
        return c;
    }

    /**
    * @dev Substracts two numbers, throws on overflow (i.e. if subtrahend is greater than minuend).
    */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        assert(b <= a);
        return a - b;
    }


    /**
    * @dev Adds two numbers, throws on overflow.
    */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        assert(c >= a);
        return c;
    }
}